package question4;

import java.util.ArrayList;
import java.util.Collections;

public class StudentApp {
    
    public static void main(String[] args) {
        FileController fc = new FileController("student.txt");
        ArrayList<String> records = fc.readLine();
        ArrayList<Student> students = new ArrayList<>();

        for (String r : records) {
            students.add(new Student(r));
        }

        for (Student s : students) {
            System.out.println(s);
        }
        
        System.out.println("========= After Sorting =========");
        
        Collections.sort(students);
        for (Student s : students) {
            System.out.println(s);
        }
    }

}