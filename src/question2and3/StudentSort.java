package question2and3;

import java.util.ArrayList;
import java.util.Collections;


//==============================================================
//Program Name:	StudentSort.java (Practical 5 Q 2)
// Description:	...
//==============================================================
//import java.util.*;

public class StudentSort {
	public static void main (String args[]){
		ArrayList<Student> students = new ArrayList<>();
		students.add(new Student("111111A", "Ivan",17));
		students.add(new Student("111111B", "Joshua", 17)); // for this code I'm 17 
		students.add(new Student("111111C", "Wen Qiang", 18));
		students.add(new Student("111111D", "Brian", 17));
		students.add(new Student("111111E", "Joshua", 18));

		Collections.sort(students);
		for (Student s : students) {
			System.out.println(s);
		}

	}
}