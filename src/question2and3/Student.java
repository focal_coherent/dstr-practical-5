package question2and3;

//================================================================
// Program Name:	Student.java (Practical 5 Q 2)
// Description:		Student class implements Comparable Interface
//================================================================
import java.util.*;

public class Student implements Comparable<Student> {

	// Declare Instance variables
	private String adminNo;
	private String name;
	private Integer age;

	public Student(String adminNo, String name, Integer age) {
		this.adminNo = adminNo;
		this.name = name;
		this.age = age;
	}

	// ----------------------------------------------------------
	// Accessor methods to get and set attributes
	// ----------------------------------------------------------
	public String getAdminNo() {
		return adminNo;
	}

	public String getName() {
		return name;
	}

	public void setAdminNo(String adminNo) {
		this.adminNo = adminNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Student s) {
		int result;
		if (name.equals(s.name))
			result = age.compareTo(s.age);
		else
			result = name.compareTo(s.name);

		return result;
	}

	@Override
	public String toString() {
		return "Student [adminNo=" + adminNo + ", name=" + name + "]";
	}

	
	
}
