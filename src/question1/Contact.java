package question1;


public class Contact implements Comparable<Contact>{
	private String firstName; 
	private String lastName;
	private String phone;
		
	
	public Contact (String firstName, String lastName, String phone){
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;		
	}
	
	
	
	public int compareTo (Contact c){
		int result;
		
		if (lastName.equals(c.lastName)) // if last name is the same
			result = firstName.compareTo(c.firstName); // compare their first names
		else
			result = lastName.compareTo(c.lastName); // else compare their last names
		return result;
	}

	@Override
	public String toString() {
		return "Contact [firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + "]";
	}
	
	
}












