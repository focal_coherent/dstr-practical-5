package question1;

import java.util.*;


public class SortPhoneList{
	//-----------------------------------------------------------------
	//  Creates an array of Contact objects, sorts them, then prints
	//  them.
	//-----------------------------------------------------------------
	public static void main (String[] args){
		ArrayList<Contact> contacts = new ArrayList<>();
		contacts.add(new Contact("Ivan", "Che", "11111111"));
		contacts.add(new Contact("Wen Qiang", "Lee", "22222222"));
		contacts.add(new Contact("Brian", "Wong", "33333333"));
		contacts.add(new Contact("Caleb", "Lee", "44444444")); // same last name

		Collections.sort(contacts);
		for (Contact c : contacts) {
			System.out.println(c);
		}
	}
}
